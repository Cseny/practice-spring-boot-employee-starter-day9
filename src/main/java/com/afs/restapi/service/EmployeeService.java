package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeJPARepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {


    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService( EmployeeJPARepository employeeJPARepository) {
        this.employeeJPARepository = employeeJPARepository;
    }

    public List<Employee> findAll() {
        return employeeJPARepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeJPARepository.findById(id).orElseThrow();
    }

    public Employee update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = employeeJPARepository.findById(id).orElseThrow();
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        return employeeJPARepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeJPARepository.findByGender(gender);
    }

    public Employee create(Employee employee) {
        return employeeJPARepository.save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return employeeJPARepository.findAll(pageable).getContent();
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}

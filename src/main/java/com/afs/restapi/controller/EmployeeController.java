package com.afs.restapi.controller;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.EmployeeService;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeResponse> getAllEmployees() {
        List<Employee> employeeList = employeeService.findAll();
        return employeeList.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeById(@PathVariable Long id) {
        return EmployeeMapper.toResponse(employeeService.findById(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public EmployeeResponse updateEmployee(@PathVariable Long id, @RequestBody EmployeeRequest request) {
        Employee updatedEmployee = employeeService.update(id, EmployeeMapper.toEntity(request));
        return EmployeeMapper.toResponse(updatedEmployee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
    }

    @GetMapping(params = "gender")
    public List<EmployeeResponse> getEmployeesByGender(@RequestParam String gender) {
        List<Employee> employees = employeeService.findAllByGender(gender);
        return employees.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeResponse createEmployee(@RequestBody EmployeeRequest employeeRequest) {
        Employee employee = employeeService.create(EmployeeMapper.toEntity(employeeRequest));
        return EmployeeMapper.toResponse(employee);
    }

    @GetMapping(params = {"page", "size"})
    public List<EmployeeResponse> findEmployeesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        List<Employee> employees = employeeService.findByPage(page, size);
        return employees.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

}

package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.CompanyService;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("companies")
@RestController
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyResponse> getAllCompanies() {
        List<Company> companies = companyService.findAll();
        return companies.stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }

    @GetMapping(params = {"page", "size"})
    public List<CompanyResponse> getCompaniesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        List<Company> companies = companyService.findByPage(page, size);
        return companies.stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public CompanyResponse getCompanyById(@PathVariable Long id) {
        Company company = companyService.findById(id);
        return CompanyMapper.toResponse(company);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CompanyResponse updateCompany(@PathVariable Long id, @RequestBody CompanyRequest company) {
        Company updatedCompany = companyService.update(id, CompanyMapper.toEntity(company));
        return CompanyMapper.toResponse(updatedCompany);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyResponse createCompany(@RequestBody CompanyRequest company) {
        Company createdCompany = companyService.create(CompanyMapper.toEntity(company));
        return CompanyMapper.toResponse(createdCompany);
    }

    @GetMapping("/{id}/employees")
    public List<EmployeeResponse> getEmployeesByCompanyId(@PathVariable Long id) {
        List<Employee> employees = companyService.findEmployeesByCompanyId(id);
        return employees.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

}

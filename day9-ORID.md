# O

- MySQL
- JPA


# R

This is a fruitful day.

# I

- A comprehensive understanding of the Controller Service Repository is sufficient.

# D

- Reviewed MySQL syntax.
- Learn to use JPA, and then use TDD to refactor the code so that it can manipulate the database through JPA.
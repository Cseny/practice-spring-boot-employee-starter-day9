# O

- Flyway
- Mapper
- Session report
- Cloud Native


# R

This is a fruitful day.

# I

- My understanding of CI/CD and Cloud Native is not yet very clear.

# D

- Flyway can help us manage the database.
- Mapper can help us map the fields required by our business to the fields in the database, which can reduce the exposure of privacy fields.
- I think I need to further understand CI/CD and Cloud Native after class.